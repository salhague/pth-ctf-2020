import re
import multiprocessing

database_log = ""
database = {
    "key1": "value1",
    "key2": "value2",
    "key3": "value3",
    "key4": "value4",
    "key5": "value5",
    "key6": "value6",
    "key7": "value7",
    "key8": "value8",
}

manager = multiprocessing.Manager()
queue = manager.Queue(1)

def remove_database(key):
    global database_log
    if key in database:
        database[key] = None
    database_log += "removed item with key: " + key + "\n"

def insert_database(key, val):
    global database_log
    database[key] = val
    database_log += "inserted item with key: " + key + " with value: " + val + "\n"

def search_database(r):
    global database_log
    items = {}
    for key in database:
        if re.match(r, key):
            items[key] = database[key]

    database_log += "searched for item: " + r + "\n"
    queue.empty()
    queue.put(items)
    return

def print_menu():
    print("SECURE DATABASE")
    print("0: insert into database")
    print("1: remove from database")
    print("2: search in database")
    print("3: exit")
    choice = 3
    try:
        choice = int(input("> "))
        if choice == 0:
            inp = input("input <key> <value>: ")
            insert_database(inp.split(" ")[0], inp.split(" ")[1])
        if choice == 1:
            remove_database(input())
        if choice == 2:
            proc = multiprocessing.Process(target=search_database, args=[input()])
            proc.start()
            proc.join(20)
            if proc.is_alive():
                proc.terminate()
                print("DOS PROTECTION ACTIVATED")
                print("DUMPING LOG FILE FOR DEBUGGING")
                print(database_log)
            else:
                print(str(queue.get()))
        if choice == 3:
            return False
    except:
        print("invalid input")
        return True
    return True


print("Welcome to my secure database solution")
print("You can store any data in my secure database")
print("I even asked an intern to add DOS protection so you can always access your information")
print("He told me he got it working flawlessly after some debugging with the log file")
insert_database("flag", "PTF{Re9EX_Do5_15_ThE_1NtErN'5_proBlem}")
remove_database("flag")

while print_menu():
    pass
