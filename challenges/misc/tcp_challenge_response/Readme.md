# [Misc] [Medium] - tcp challenge response
## Description
the user is confronted with a series of challenges to answer, in current configuration a random number of challenges and a random order 

## Author(s)
- Ian Schön

## ToDo:
- Include better CTFd text

## How to build/host
- Docker image
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Docker compile commands
see dockerfile

### Text instructions
> TODO

### File on CTFd
run docker image and expose port 9999 by default configuration

### Hint
> programming is key, there are not that many different types

## Solution
<details>
<summary>Click to show</summary>
see `client/client.py`
</details>

### Flag
```PTH{7cp_15_fun_4nd_50_14_4_n1c3_ch4ll3n63}```
