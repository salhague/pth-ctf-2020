from random import randint

HOST = '0.0.0.0'  # the interface the challenge is running on
PORT = 9999  # the host port trough which the challenge is available
FLAG = "PTH{7cp_15_fun_4nd_50_14_4_n1c3_ch4ll3n63}"  # the flag to be won
ROUND_DURATION = 3  # time for a round in seconds
STAGE_COUNT = lambda: randint(100, 999)  # amount of stages to be generated  (lambda or int value)
CONNECTIONS = 50  # amount of simultaneous connections

DEBUG = False  # debug mode True or False
VERBOSE = False  # Verbose True or False
