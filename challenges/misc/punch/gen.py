#!/usr/bin/python3

from punchlib import encode
import os
from zipfile import *

def createCard(line):
    assert len(line) <= 80
    cardDots = list()
    for char in line:
        cardDots.append(encode(char))
    return cardDots

def generateAsciiCard(dots, symbol):
    cardAscii = ''
    cardAscii += '   '+'_'*len(dots)+'\n'
    cardAscii += '12|'+''.join([(symbol if num.count(12) else " ") for num in dots])+'\n'
    cardAscii += '11|'+''.join([(symbol if num.count(11) else " ") for num in dots])+'\n'
    cardAscii += ' 0|'+''.join([(symbol if num.count(0) else " ") for num in dots])+'\n'
    cardAscii += ' 1|'+''.join([(symbol if num.count(1) else " ") for num in dots])+'\n'
    cardAscii += ' 2|'+''.join([(symbol if num.count(2) else " ") for num in dots])+'\n'
    cardAscii += ' 3|'+''.join([(symbol if num.count(3) else " ") for num in dots])+'\n'
    cardAscii += ' 4|'+''.join([(symbol if num.count(4) else " ") for num in dots])+'\n'
    cardAscii += ' 5|'+''.join([(symbol if num.count(5) else " ") for num in dots])+'\n'
    cardAscii += ' 6|'+''.join([(symbol if num.count(6) else " ") for num in dots])+'\n'
    cardAscii += ' 7|'+''.join([(symbol if num.count(7) else " ") for num in dots])+'\n'
    cardAscii += ' 8|'+''.join([(symbol if num.count(8) else " ") for num in dots])+'\n'
    cardAscii += ' 9|'+''.join([(symbol if num.count(9) else " ") for num in dots])+'\n'
    cardAscii += '  |'+'_'*len(dots)+'\n'
    return cardAscii

def writeToFile(data, filename):
    path = 'cards/'+filename
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w+') as cardFile:
        cardFile.writelines(data)

def generateCardsFromFile(file):
    with open(file, 'r') as sourceCode:
        lines = sourceCode.readlines()

    n = 1
    for cnt, line in enumerate(lines):
        line = line.rstrip().upper()
        subcards = [line[i:i+80] for i in range(0, len(line), 80)]
        for subline in subcards:
            card = createCard(subline)
            asciiCard = generateAsciiCard(card, "x")
            writeToFile(asciiCard, str.format('card{0}.txt', n))
            n+=1
   
def zipOutput():
    with ZipFile('cards.zip', 'w') as zipObj:
        for (dirpath, dirnames, filenames) in os.walk('cards/'):
            for filename in filenames:
                zipObj.write('cards/'+filename)    

if __name__ == "__main__":
    generateCardsFromFile('SOURCE.F95')    
    zipOutput()