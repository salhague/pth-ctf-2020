# [Misc] [Medium] - The smallest challenge
## Description
Python jail -> help() -> pager -> command exec

## Author(s)
- PrincePanda

## How to build/host
- Docker image
- Text instructions on CTFd

### Docker compile commands
```
Docker setup instructions
```

### Text instructions
> This challenge is so small i can just give the code here

```python
i = input()
assert(len(i) == 4)
eval(i + "()")
```

## Solution
<details>
<summary>Click to show</summary>

type help
open the help page of some python function
execute "!/bin/bash" in the opened pager

<pre><code>snippit of code</code></pre>

</details>

### Flag
```PTF{PyTh0n_h3lP!_L3Ss_H4se_c0D3_3x3cUT10N}```
