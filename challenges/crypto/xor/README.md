# [Crypto] [Easy] - XOR
## Description
Simple XOR warmup challenge.

## Author(s)
- RPScylla

## ToDo:
- Include better CTFd text

## How to build/host
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Text instructions
> Todo

### File on CTFd
```
To Generate the flag change the `flag` and `key` file and run `./generate.py` output is saved in `encrypted`.
`flag` should be 5 characters long and `key` should be a multiple of 5 characters.
```
- `generate.py`
- `encrypted`

### Hint
> Try looking for the characters you already know

## Solution
<details>
<summary>Click to show</summary>
<p>

We know because the given `generate.py`:
- Key is alphanumeric,
- It has a length of 5 characters
- Message starts with `PTH{` and ends with `}`

Given the XOR properties we can figure out candidates for the encrypted messages in a sliding window manner.
Because the key is 5 long and we know 5 characters from different (modulo) positions of the message already we can reverse it to the key.

see `poc.py` for implementation.

</p>
</details>

### Flag
```PTH{x0r_15_k3y_70_3ncryp710n!}```