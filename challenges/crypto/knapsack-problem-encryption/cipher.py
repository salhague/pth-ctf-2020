#!/usr/bin/python3

from egcd import egcd
from secret import flag
from secret import public_key

class Cipher:
    def encrypt(self, data, l):
        s = 0
        for i, b in enumerate(data):
            s += int(b) * l[i]
        return s

    def decrypt(self, data, m, n, l):
        _, inv, _ = egcd(m, n)
        d = (data * inv) % n

        bits = ""
        for i in l[::-1]:
            if i <= d:
                d = d - i
                bits += '1'
            else:
                bits += '0'
        if d != 0:
            print("error")
            exit()
        return int(bits[::-1], 2)


if __name__ == "__main__":
    cipher = Cipher()
    c = cipher.encrypt(flag, public_key)
    with open('out.txt', 'w+') as outFile:
        outFile.write("p = %s\n" % public_key)
        outFile.write("c = %s\n" % c)