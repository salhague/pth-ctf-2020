# [General Skills] [Easy] - Grep-r
## Description
Simple introduction to test grep -r

## Author(s)
- RPScylla

## ToDo:
- Fix flag
- Better CTFd Text

## How to build/host
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### Text instructions
> In one of these files is the flag?

### File on CTFd
``` 
Generate using `./generate.py` 
```
- `files.zip`

### Hint
> Try filtering using grep

## Solution
<details>
<summary>Click to show</summary>
<p>

- `grep -r PTH{ ./files`

</p>
</details>

### Flag
```PTH{flag}```
