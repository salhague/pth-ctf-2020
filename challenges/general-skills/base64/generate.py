#!/usr/bin/python3
import base64

with open('flag', 'r') as flagFile:
    flag = flagFile.read().encode('utf-8')
    base64_bytes = flag
    for x in range(0, 49):
        base64_bytes = base64.b64encode(base64_bytes)
    output = open("output.txt","w+")
    output.write(str(base64_bytes, 'utf-8'))
    output.close()
