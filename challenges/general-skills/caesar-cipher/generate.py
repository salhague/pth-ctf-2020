#!/usr/bin/python3
import string

def caesar(plaintext, shift):
    alphabetLower = string.ascii_lowercase
    alphabetUpper = string.ascii_uppercase
    shifted_alphabetLower = alphabetLower[shift:] + alphabetLower[:shift]
    shifted_alphabetUpper = alphabetUpper[shift:] + alphabetUpper[:shift]
    tableLower = str.maketrans(alphabetLower, shifted_alphabetLower)
    tableUpper = str.maketrans(alphabetUpper, shifted_alphabetUpper)
    return plaintext.translate(tableLower).translate(tableUpper)

with open('flag', 'r') as flagFile:
    flag = flagFile.read()
    caesarOutput = caesar(flag, 18)
    
    output = open("output.txt","w+")
    output.write(caesarOutput)
    output.close()
