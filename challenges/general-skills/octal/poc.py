#!/usr/bin/python3

def fromOcatl(ocatl_str):
    str_converted = ""
    for octal_char in ocatl_str.split(" "):
        str_converted += chr(int(octal_char, 8))
    return str_converted

with open('output.txt', 'r') as octalFile:
    octalInput = octalFile.read()
    flag = fromOcatl(octalInput)
    print (flag)
