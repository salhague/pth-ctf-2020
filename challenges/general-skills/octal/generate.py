#!/usr/bin/python3
def toOcatl(string):
    retval = ''
    for char in string:
        retval += oct(ord(char))[2:] + " "
    return retval[:-1]

with open('flag', 'r') as flagFile:
    flag = flagFile.read()
    octal = toOcatl(flag)
    output = open("output.txt","w+")
    output.write(octal)
    output.close()
