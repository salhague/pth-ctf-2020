#!/usr/bin/python3

import string
import random

def random_string(length):
        def random_char():
            return random.choice(string.ascii_letters + string.digits + "{_}")
        return ''.join(random_char() for _ in range(length))

with open('flag', 'r') as flagFile:
    flag = flagFile.read()    
    with open("file.txt","w+") as outputFile:
        for x in range(0, 1684):
            outputFile.write(random_string(random.randint(10,30))+"\n")
        outputFile.write(flag+"\n")
        for x in range(0, 987):
            outputFile.write(random_string(random.randint(10,30))+"\n")
    
