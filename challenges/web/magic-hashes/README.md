# Web Easy - Magic Hashes
## Description
magic hashes challenge.

## Author(s)
- RPScylla

## ToDo:
- Make better webpage
- Include better flag
- Better CTFd text

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t magic-hashes .
sudo docker run -d -it --rm -p 8080:80 magic-hashes
```

### Text instructions
> Can you login?

### Hint
> This look like magic

## Solution
<details>
<summary>Click to show</summary>
<p>

PHP magic hashes, == instead of ===
md5(hash1) == md5(hash2)
0e[0-9]+ == 0e[0-9]+ : true

PTH magic hashes:
- PTHfkToH : 0e564929597359528626655500419413
- PTHAEh9J1 : 0e435703544781679795716027852808
- PTHAAAroqrS : 0e626100885060322729075656545324
- PTHAAA2aKnR : 0e559586996955833938750849113498
- PTHAABEaHam : 0e235518494280839131957539462077
- PTHAABISYW5 : 0e541220816610369406074976165429
- PTHAABjqGgL : 0e720852550565178042680995960711
- PTHAAAAAfRqgP : 0e955227571722795067766958909882
- PTHAAAAA42Xno : 0e277176237732038530285880802725
- PTHAAAABaVAsX : 0e463455175985769675671779028287
- PTHAAAABqekmi : 0e185636468141368259953077575960

Solve:
- enter any magic md5 hash other than `PTHAEh9J1` like `PTHAABjqGgL`

</p>
</details>

### Flag
PTH{md5_magic_hashes}