<?php
$flag = "PTH{fr0m_c4v3m4n_70_h4ck3r}";
$dolls = 1;
require("chall.php");
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Evolution</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>

<body>

    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Evolution</a>
        </nav>
    </header>

    <!-- Begin page content -->
    <main role="main" class="container">
        <div class="row justify-content-center">
            <?php
                if(!check_chall1())
                {
                    $dolls = 1;
                    print_chall1();
                }
                else if(!check_chall2())
                {
                    $dolls = 2;
                    print_chall2();
                }
                else if(!check_chall3())
                {
                    $dolls = 3;
                    print_chall3();
                }
                else if(!check_chall4())
                {
                    $dolls = 4;
                    print_chall4();
                }
                else if(!check_chall5())
                {
                    $dolls = 5;
                    print_chall5();
                }
                else
                {
                    $dolls = 6;
                    print_flag();
                }
            ?>
        </div>
        <div class="row justify-content-center">
            <div class="col">
                <img src="img/<?php echo $dolls; ?>.png" alt="dolls" />
            </div>
        </div>
        <div class="row justify-content-center">
            <form action="" method="POST">
                <button type="submit" id="reset" name="reset" value="True" class="btn btn-danger">reset!</button>
            </form>
        </div>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">Place sticky footer content here.</span>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery-slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>