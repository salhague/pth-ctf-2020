# [Forensics] [Easy] - Image diff
## Description
Simple image diff challenge

## Author(s)
- RPScylla

## ToDo:
- Include better CTFd text

## How to build/host
- Text instructions on CTFd
- File on CTFd
- Hint on CTFd

### File on CTFd
```
To Generate the flag change the `flag` and `firewall.bmp` file and run `./generate.py` output is saved in `firewall2.bmp`
```
- `firewall.bmp`
- `firewall2.bmp`

### Text instructions
> We found this two images, can you spot the difference?

### Hint
> look at the binary values

## Solution
<details>
<summary>Click to show</summary>
<p>

see `poc.py`

</p>
</details>

### Flag
```PTH{c4n_y0u_f1nd_7h3_d1ff3r3nc3}```