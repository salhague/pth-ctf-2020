#!/usr/bin/env python3
'''
Script to generate modbus traffic containing the flag to conpot honeypot.

pip3 install pymodbus
'''

__author__ = "PentestHub"
__license__ = "MIT"
__version__ = "1.0"

from pymodbus.client.sync import ModbusTcpClient
import time

def split(word): 
    return [int(char) for char in word]  

flag = "PTH{talking_about_old_protocols}"
binary = ' '.join(format(ord(x), 'b') for x in flag)
binary_list = binary.split (" ")

client = ModbusTcpClient('10.0.2.121', '502')
while True:
    for char in binary_list:
        char_1 = split(char)
        bool_list = [bool(x) for x in char_1]
        print(bool_list)
        client.write_coils(1, bool_list, unit=0x01)
        time.sleep(2)
        client.read_coils(1, 7, unit=0x01)
        time.sleep(4)
client.close()